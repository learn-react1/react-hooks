import React, { useReducer } from 'react'
import './App.css'
import ClassCounter from './components/ClassCounter'
import HookCounter from './components/HookCounter'
import HookCounterTwo from './components/HookCounterTwo'
import HookCounterThree from './components/HookCounterThree'
import HookCounterFour from './components/HookCounterFour'
import ClassCounterOne from './components/ClassCounterOne'
import HookCounterOne from './components/HookCounterOne'
import ClassMouse from './components/ClassMouse'
import HookMouse from './components/HookMouse'
import MouseContainer from './components/MouseContainer'
import IntervallClassCounter from './components/IntervalClassCounter'
import IntervalHookCounter from './components/IntervalHookCounter'
import DataFetching from './components/DataFetching'
import ComponentD from './components/ComponentD'
import CounterOne from './components/CounterOne'
import CounterTwo from './components/CounterTwo'
import CounterThree from './components/CounterThree'
import ComponentA from './components/ComponentA'
import ComponentB from './components/ComponentB'
import ComponentC from './components/ComponentC'
import DataFetchingOne from './components/DataFetchingOne'
import DataFetchingTwo from './components/DataFetchingTwo'
import ParentComponent from './components/ParentComponent'
import Counter from './components/Counter'
import FocusInput from './components/FocusInput'
import ClassTimer from './components/ClassTimer'
import HookTimer from './components/HookTimer'
import DocTitleOne from './components/DocTitleOne'
import DocTitleTwo from './components/DocTitleTwo'
import CounterOneHook from './components/CounterOneHook'
import CounterTwoHook from './components/CounterTwoHook'
import UserForm from './components/UserForm'

// 1. Create the user context.
// 2. Wrap the component with the created context.
// 3. Export the context and import it in the component.
export const UserContext = React.createContext()
export const ChannelContext = React.createContext()

// 1. Create initial state and reducer function
// 2. use `useReducer` to create the state and dispatch function
// within the App Component.
// 3. Create a context for the counter
// 4. Wrap all components that should have access to the context
// within in the context provider and pass an object containing
// the state and the dispatch method as value.
export const CountContext = React.createContext()

const initialState = 0
const reducer = (state, action) => {
	switch (action) {
		case 'increment':
			return state + 1
		case 'decrement':
			return state - 1
		case 'reset':
			return initialState
		default:
			return state
	}
}

function App() {
	const [count, dispatch] = useReducer(reducer, initialState)
	return (
		<CountContext.Provider
			value={{ countState: count, countDispatch: dispatch }}
		>
			<div className="App">
				{/* <ClassCounter /> */}
				{/* <HookCounter /> */}
				{/* <HookCounterTwo /> */}
				{/* <HookCounterThree /> */}
				{/* <HookCounterFour /> */}
				{/* <ClassCounterOne /> */}
				{/* <HookCounterOne /> */}
				{/* <ClassMouse /> */}
				{/* <HookMouse /> */}
				{/* <MouseContainer /> */}
				{/* <IntervallClassCounter /> */}
				{/* <IntervalHookCounter /> */}
				{/* <DataFetching /> */}
				{/* <UserContext.Provider value={'Mirko'}>
				<ChannelContext.Provider value={'DefaultChannel'}>
					<ComponentD />
				</ChannelContext.Provider>
			</UserContext.Provider> */}
				{/* <CounterOne /> */}
				{/* <CounterTwo /> */}
				{/* <CounterThree /> */}
				{/* Count - {count}
				<ComponentA />
				<ComponentB />
				<ComponentC /> */}
				{/* <DataFetchingOne /> */}
				{/* <DataFetchingTwo /> */}
				{/* <ParentComponent /> */}
				{/* <Counter /> */}
				{/* <FocusInput /> */}
				{/* <ClassTimer />
				<HookTimer /> */}
				{/* <DocTitleOne />
				<DocTitleTwo /> */}
				{/* <CounterOneHook />
				<CounterTwoHook /> */}
				<UserForm />
			</div>
		</CountContext.Provider>
	)
}

export default App
