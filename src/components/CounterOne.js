import React, { useReducer } from 'react'

// 1. import useReducer
// 2. define initial state and reducer function.
// 3. Get hold of value to display in jsx by getting
// the state and dispatch function from useReducer which we can use
// as function on the buttons.

const initialState = 0
const reducer = (state, action) => {
	switch (action) {
		case 'increment':
			return state + 1
		case 'decrement':
			return state - 1
		case 'reset':
			return initialState
		default:
			return state
	}
}

function CounterOne() {
	const [count, dispatch] = useReducer(reducer, initialState)

	return (
		<div>
			<div>Count - {count}</div>
			<button onClick={() => dispatch('increment')}>Increment</button>
			<button onClick={() => dispatch('decrement')}>Dercrement</button>
			<button onClick={() => dispatch('reset')}>Reset</button>
		</div>
	)
}

export default CounterOne
