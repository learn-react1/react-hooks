import React, { useContext } from 'react'
import { CountContext } from '../App'

function ComponentF() {
	const countContext = useContext(CountContext)
	return (
		<div>
			Compnent F - {countContext.countState}
			<button onClick={() => countContext.countDispatch('increment')}>
				Increment
			</button>
			<button onClick={() => countContext.countDispatch('decrement')}>
				Dercrement
			</button>
			<button onClick={() => countContext.countDispatch('reset')}>Reset</button>
		</div>
	)
}

export default ComponentF
