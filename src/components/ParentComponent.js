import React, { useState, useCallback } from 'react'
import Count from './Count'
import Button from './Button'
import Title from './Title'

// A new increment salary function is created, each time this parent
// component is re-rendered.
// The same for incrementAge. These two new functions are `new` props
// for the Button component and trigger a re-render of the Button
// component!

// Summary: Re-rendering of parent component creates new functions
// that are passed as props to children components. These new functions
// are handles as new props and trigger a re-render of the children
// components as well.
// To prevent this, use hook `useCallback`, which cashes a function.
// The parent component will use the cached functions instead of creating
// new functions and this prevents a re-render of the children components
// because the prop function has changed.

// useCallback is a hook that will return a memoized version of the callback
// function that only changes if one of the dependencies has changed.

function ParentComponent() {
	const [age, setAge] = useState(25)
	const [salary, setSalaray] = useState(5000)

	const incrementAge = useCallback(() => {
		setAge(age + 1)
	}, [age])

	const incrementSalary = useCallback(() => {
		setSalaray(salary + 1000)
	}, [salary])

	return (
		<div>
			<Title />
			<Count text="Age" count={age} />
			<Button handleClick={incrementAge}>Increment Age</Button>
			<Count text="Salary" count={salary} />
			<Button handleClick={incrementSalary}>Increment Salary</Button>
		</div>
	)
}

export default ParentComponent
