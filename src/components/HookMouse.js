import React, { useState, useEffect } from 'react'

function HookMouse() {
	const [x, setX] = useState(0)
	const [y, setY] = useState(0)

	const logMousePosition = (e) => {
		console.log('Mouse event')
		setX(e.clientX)
		setY(e.clientY)
	}

	// Without the dependency array, the effect is called after each re-render, which results
	// in add event listeners each time.
	// When the effect does not depend on a prop or state, an empty dependency array will call
	// the effect ONCE on initial render.
	// The problem here is, if the component is unmounted, the added eventListener is not removed
	// and continues to log.
	// To solve this, useEffect can return a function, which is executed, when
	// the component is unmounted.
	useEffect(() => {
		console.log('useEffect called')
		window.addEventListener('mousemove', logMousePosition)

		return () => {
			console.log('Component unmounting code')
			window.removeEventListener('mousemove', logMousePosition)
		}
	}, [])

	return (
		<div>
			Hooks X - {x}, Y - {y}
		</div>
	)
}

export default HookMouse
