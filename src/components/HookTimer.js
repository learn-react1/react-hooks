import React, { useState, useEffect, useRef } from 'react'

/* useRef can also hold any mutable value.
The value will persist through the re-renders AND
it will not trigger a re-render, when the data it stores changes.
*/

function HookTimer() {
	const [timer, setTimer] = useState(0)
	const intervalRef = useRef()

	useEffect(() => {
		intervalRef.current = setInterval(() => {
			setTimer((prevTimer) => prevTimer + 1)
		}, 1000)

		// Return function is called when component unmounts.
		return () => {
			clearInterval(intervalRef.current)
		}
	}, [])
	return (
		<div>
			Hook Timer - {timer}
			<button onClick={() => clearInterval(intervalRef.current)}>
				Clear Hook Timer
			</button>
		</div>
	)
}

export default HookTimer
