import React, { useState, useEffect } from 'react'

function HookCounterOne() {
	const [count, setCount] = useState(0)
	const [name, setName] = useState('')

	// Parameter of useEffect is a function.
	// Use effect runs after each render of the component.
	// To prevent this, add a second parameter, an array, that React has to watch.
	// Conditionally call useEffect, if those watched dependencies parameters change.
	useEffect(() => {
		console.log('useEffect - updating document title')
		document.title = `Clicked ${count} times`
	}, [count])

	return (
		<div>
			<input
				type="text"
				value={name}
				onChange={(e) => setName(e.target.value)}
			/>
			<button onClick={() => setCount(count + 1)}>Clicked {count} times</button>
		</div>
	)
}

export default HookCounterOne
