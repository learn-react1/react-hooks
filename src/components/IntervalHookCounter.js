import React, { useState, useEffect } from 'react'

function IntervalHookCounter() {
	const [count, setCount] = useState(0)

	const tick = () => {
		setCount((prevCount) => prevCount + 1)
	}

	// The dependency array should not be viewed as WHEN React needs to run the effect,
	// but rather WHAT useEffect has to watch for changes.
	// Therefore we have to watch `count` as well.
	// Alternatively you can use the second form of `setCount` and pass a function,
	// that takes the previous value as parameter.
	useEffect(() => {
		const interval = setInterval(tick, 1000)

		return () => {
			clearInterval(interval)
		}
	}, [])

	return <h1>{count}</h1>
}

export default IntervalHookCounter
